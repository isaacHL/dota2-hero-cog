import discord
from discord.ext import commands

class Dota2Hero:
    """My custom cog that does stuff!"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def dota2hero(self, user : discord.Member):
        """This does stuff!"""
        message = user + "respawned as Wisp."
        #Your code will go here
        await self.bot.say(message)

def setup(bot):
    bot.add_cog(Dota2Hero(bot))
