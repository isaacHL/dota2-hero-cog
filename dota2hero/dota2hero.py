import discord
import random
from discord.ext import commands
import dota2api
from .utils.dataIO import fileIO


class Dota2Hero:
    """My custom cog that does stuff!"""

    def __init__(self, bot):
        self.bot = bot
        self.dota_players = fileIO("data/dota2hero/playerdata.json", "load")
        self.api = dota2api.Initialise()
        self.key = True
        self.heroes = self.api.get_heroes()

    @commands.group(pass_context=True)
    async def dota2(self, ctx):
        """Returns various data for dota players"""

        if ctx.invoked_subcommand is None:
            await self.bot.say("Type help dota2 for info.")

    @dota2.command(name='players', pass_context=True)
    async def players(self, ctx):
        """Returns list of players and their account id"""

    @commands.group(pass_context=True, invoke_without_command=True)
    async def dota2hero(self, ctx, user: discord.Member=None):
        """This does stuff!"""
        heroes = self.heroes["heroes"]
        heroesLength = len(heroes)
        randomHeroIndex = random.randint(0, heroesLength)
        randomHero = heroes[randomHeroIndex]["localized_name"]
        msg = " respawns as: " + randomHero

        if user == None:
            out_message = ctx.message.author.mention + msg
        else:
            out_message = user.mention + msg
        # Your code will go here
        await self.bot.say(out_message)


def setup(bot):
    bot.add_cog(Dota2Hero(bot))
